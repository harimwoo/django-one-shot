from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def show_list(request):
    lists = TodoList.objects.all()
    context = {
        "list_objects": lists,
    }
    return render(request, "todos/list.html", context)


def show_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {"list_object": list}
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "list_object": list,
        "list_form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            item_id = form.cleaned_data["list"].id
            item.save()
            return redirect("todo_list_detail", id=item_id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            instance_id = form.cleaned_data["list"].id
            form.save()
            return redirect("todo_list_detail", id=instance_id)
    else:
        form = TodoItemForm(instance=item)
    context = {"form": form}
    return render(request, "todos/edit_item.html", context)
